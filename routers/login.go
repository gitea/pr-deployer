package routers

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"time"

	"gitea.com/gitea/pr-deployer/pkgs/proxy"
	"gitea.com/gitea/pr-deployer/pkgs/settings"
	"gitea.com/go-chi/session"
	log "github.com/sirupsen/logrus"
	"golang.org/x/oauth2"
)

func Home(w http.ResponseWriter, r *http.Request) {
	sess := session.GetSession(r)
	user := sess.Get("user")
	oriState := "111111" // TODO: use a random

	if user == nil {
		sess.Set("state", oriState)

		url := settings.OAuth2Conf.AuthCodeURL(oriState, oauth2.AccessTypeOffline)
		fmt.Printf("Visit the URL for the auth dialog: %v", url)

		http.Redirect(w, r, url, http.StatusFound)
		return
	}

	http.Redirect(w, r, "/prs", http.StatusFound)
}

func OAuth2Callback(w http.ResponseWriter, r *http.Request) {
	sess := session.GetSession(r)
	oriState := sess.Get("state")
	state := r.FormValue("state")
	if state != oriState {
		log.Error("state is not equal")
		return
	}

	ctx := context.Background()
	httpClient := &http.Client{
		Timeout: 20 * time.Second,
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
			Proxy: func(req *http.Request) (*url.URL, error) {
				return proxy.Proxy()(req)
			},
		},
	}
	ctx = context.WithValue(ctx, oauth2.HTTPClient, httpClient)

	code := r.FormValue("code")
	tok, err := settings.OAuth2Conf.Exchange(ctx, code)
	if err != nil {
		log.Error(err)
		return
	}
	fmt.Println(tok)
	sess.Set("token", tok)

	client := settings.OAuth2Conf.Client(ctx, tok)
	var url string
	if settings.ServiceType == "gitea" {
		url = settings.OAuth2ServerURL + "/api/v1/user"
	} else if settings.ServiceType == "github" {
		url = "https://api.github.com/user"
	}
	resp, err := client.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	bs, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(string(bs))

	var user = make(map[string]interface{})
	err = json.NewDecoder(bytes.NewReader(bs)).Decode(&user)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(user)
	sess.Set("user", user)

	http.Redirect(w, r, "/", 302)
}
