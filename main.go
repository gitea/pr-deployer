package main

import (
	"gitea.com/gitea/pr-deployer/cmd"
	"gitea.com/gitea/pr-deployer/models"
	"gitea.com/gitea/pr-deployer/pkgs/settings"

	log "github.com/sirupsen/logrus"
)

func main() {
	log.SetLevel(log.TraceLevel)

	if err := settings.Init(); err != nil {
		log.Fatal(err)
	}
	log.Info("config loaded")

	if err := models.Init(); err != nil {
		log.Fatal(err)
	}
	log.Info("models init")

	if err := cmd.Execute(getPublicAssets(), getTemplateAssets()); err != nil {
		log.Fatal(err)
	}
}
