//go:build !bindata
// +build !bindata

package main

import (
	"io/fs"
	"os"
)

func getPublicAssets() fs.FS {
	return os.DirFS("public/")
}

func getTemplateAssets() fs.FS {
	return os.DirFS("templates/")
}
