# PR-Deployer help Gitea to run some PR as a gitea instance for easier testing.

Since sometiems we need to check if the PR is like what expected in the UI and don't 
want to run it locally, We can deployer the PR as a service as https://try-pr-{index}.gitea.io. 
If the process is almost automatically, it will help all contributors.

The project aims to do that. It will allow the users who has write permission to 
the repository to start or stop a PR service via a web interface. (If the web interface
supports mobile that's awesome.)

## Flow

![Flow of the deployment](docs/flow.jpg)

## Requirement

- OAuth2 Application client code and token for Github or Gitea
- A personal access token for Github or Gitea
- A token to visit cloudflare API

## Installation

`go install gitea.com/gitea/pr-deployer`

## Running

`pr-deployer`