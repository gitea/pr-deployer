//go:build bindata
// +build bindata

package main

import (
	"embed"
	"io/fs"

	log "github.com/sirupsen/logrus"
)

//go:embed public/*
var publicAssets embed.FS

//go:embed templates/*
var templateAssets embed.FS

func getPublicAssets() fs.FS {
	res, err := fs.Sub(publicAssets, "public")
	if err != nil {
		log.Fatal(err)
	}
	return res
}

func getTemplateAssets() fs.FS {
	res, err := fs.Sub(templateAssets, "templates")
	if err != nil {
		log.Fatal(err)
	}
	return res
}
