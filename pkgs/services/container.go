package services

import (
	"bytes"
	"context"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"

	"gitea.com/gitea/pr-deployer/pkgs/settings"

	"github.com/pkg/errors"
)

func getImageTag(number int) string {
	return fmt.Sprintf("pr-%d", number)
}

// buildImage build the docker image via Dockerfile
func buildImage(ctx context.Context, number int) (string, error) {
	p := getPRGitDir(number)
	tagName := fmt.Sprintf("%s/%s:%s", settings.RepoOwner, settings.RepoName, getImageTag(number))
	cmd := exec.Command("docker", "build", "-t",
		tagName,
		".",
	)
	if err := WithDir(cmd, p).Run(); err != nil {
		return "", errors.Wrap(err, "docker build")
	}

	return tagName, nil
}

func contractErr(errs ...error) error {
	var errContent string
	for _, e := range errs {
		if e != nil {
			errContent += e.Error() + ";"
		}
	}
	return errors.New(errContent)
}

func runImage(ctx context.Context, number int, image string) (string, error) {
	p := getPRRuntimeDir(number)
	if err := os.MkdirAll(p, os.ModePerm); err != nil {
		return "", err
	}

	composeDir := filepath.Dir(settings.DockerComposeTemplateFile)

	var buf bytes.Buffer
	var errBuf bytes.Buffer
	cmd := exec.Command("docker-compose", "up", "-d")
	cmd.Env = append(os.Environ(),
		"DEMO_CONTAINER="+image,
		"DEMO_DOMAIN="+getFullSubDomain(number),
		"DEMO_SSH=22",
		"DEMO_DATA_DIR="+p,
	)
	cmd.Stdout = &buf
	cmd.Stderr = &errBuf
	err := WithDir(cmd, composeDir).Run()
	if err != nil || errBuf.String() != "" {
		err = contractErr(err, errors.New(errBuf.String()))
		return "", errors.Wrap(err, "docker run")
	}

	return buf.String(), nil
}
