package services

import (
	"context"
	"fmt"

	"gitea.com/gitea/pr-deployer/pkgs/settings"

	"github.com/google/go-github/v39/github"
	"golang.org/x/oauth2"
)

type Client interface {
	CheckWebhook(ctx context.Context) error
	UpdateCommitStatus(ctx context.Context, number int, sha, status, desc, targetURL string) error
	GetPullRequests(p int) ([]*github.PullRequest, error)
}

func NewClient(token *oauth2.Token) (Client, error) {
	if settings.ServiceType == "github" {
		return NewGithubClient(token), nil
	}

	return nil, fmt.Errorf("unsupported service type: %s", settings.ServiceType)
}
