package services

import (
	"context"
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"

	"gitea.com/gitea/pr-deployer/models"
	"gitea.com/gitea/pr-deployer/pkgs/proxy"
	"gitea.com/gitea/pr-deployer/pkgs/settings"

	"github.com/google/go-github/v39/github"
	log "github.com/sirupsen/logrus"
	"golang.org/x/oauth2"
)

type GithubClient struct {
	client *github.Client
}

func NewGithubClient(token *oauth2.Token) *GithubClient {
	//ts := settings.OAuth2Conf.TokenSource(context.Background(), token)
	//ts := oauth2.StaticTokenSource(token)
	var client = &http.Client{
		Transport: &oauth2.Transport{
			Base: &http.Transport{
				TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
				Proxy: func(req *http.Request) (*url.URL, error) {
					return proxy.Proxy()(req)
				},
			},
			Source: oauth2.ReuseTokenSource(token, nil),
		},
	}

	githubClient := github.NewClient(client)
	if settings.BaseURL != "https://github.com" {
		githubClient, _ = github.NewEnterpriseClient(settings.BaseURL, settings.BaseURL, client)
	}

	return &GithubClient{
		client: githubClient,
	}
}

func (c *GithubClient) CheckWebhook(ctx context.Context) error {
	hookIDStr, err := models.GetSystemConfig("webhook_id")
	if err != nil {
		return err
	}
	if hookIDStr != "" {
		hookID, _ := strconv.ParseInt(hookIDStr, 10, 64)
		_, resp, err := c.client.Repositories.GetHook(ctx, settings.RepoOwner, settings.RepoName, hookID)
		if err == nil {
			return nil
		}

		if resp.StatusCode != 404 {
			return err
		}
	}

	// check webhook registry
	hook, resp, err := c.client.Repositories.CreateHook(context.Background(), settings.RepoOwner, settings.RepoName, &github.Hook{
		Events: []string{"pull_request"},
		Active: github.Bool(true),
		Config: map[string]interface{}{
			"content_type": "json",
			"insecure_ssl": "1",
			"secret":       string(settings.WebhookSecretKey),
			"url":          fmt.Sprintf("https://%s/webhook", settings.Domain),
		},
	})
	if err != nil {
		bs, err1 := ioutil.ReadAll(resp.Body)
		if err1 != nil {
			log.Error(err1)
		}
		fmt.Println("======", string(bs))

		return err
	}

	return models.SaveSystemConfig("webhook_id", fmt.Sprintf("%d", *hook.ID))
}

func (c *GithubClient) UpdateCommitStatus(ctx context.Context, number int, sha, status, desc, targetURL string) error {
	// pending, success, error, or failure
	_, _, err := c.client.Repositories.CreateStatus(ctx, settings.RepoOwner, settings.RepoName, sha, &github.RepoStatus{
		State:       github.String(status),
		TargetURL:   github.String(targetURL),
		Description: github.String(desc),
		Context:     github.String("pr-deployer"),
		// AvatarURL:
	})
	return err
}

func (c *GithubClient) GetPullRequests(p int) ([]*github.PullRequest, error) {
	pulls, _, err := c.client.PullRequests.List(context.Background(), settings.RepoOwner, settings.RepoName, &github.PullRequestListOptions{
		Sort:      "updated",
		Direction: "desc",
		ListOptions: github.ListOptions{
			Page:    p,
			PerPage: 50,
		},
	})

	return pulls, err
}
