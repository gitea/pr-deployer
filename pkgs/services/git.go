package services

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"strings"

	"gitea.com/gitea/pr-deployer/pkgs/settings"

	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

func updateGitRepo(number int, sha string) (string, error) {
	p := getPRGitDir(number)
	log.Trace("clone code into", p)

	var local = fmt.Sprintf("pull/%d/head:pr/%d", number, number)
	st, err := os.Stat(p)
	if err != nil {
		if !os.IsNotExist(err) {
			return "", err
		}

		if err := exec.Command("git", "clone", settings.RepoURL, p).Run(); err != nil {
			return "", fmt.Errorf("git clone: %v", err)
		}

		cmd := exec.Command("git", "fetch", "origin", local)
		if err := WithDir(cmd, p).Run(); err != nil {
			return "", errors.Wrap(err, "fetch")
		}
	} else if !st.IsDir() {
		return "", fmt.Errorf("%s is a file but not a dir", p)
	}

	cmd := exec.Command("git", "pull", "origin", local)
	if err := WithDir(cmd, p).Run(); err != nil {
		return "", errors.Wrap(err, fmt.Sprintf("checkout %s", sha))
	}

	var branchName = fmt.Sprintf("pr/%d", number)
	var newSHA = sha

	if sha != "" {
		cmd := exec.Command("git", "checkout", sha)
		if err := WithDir(cmd, p).Run(); err != nil {
			return "", errors.Wrap(err, fmt.Sprintf("checkout %s", sha))
		}
		newSHA = sha
	} else {
		cmd := exec.Command("git", "rev-parse", "--abbrev-ref", "HEAD")
		cmd.Dir = p
		var buf bytes.Buffer
		cmd.Stdout = &buf
		if err := cmd.Run(); err != nil {
			return "", fmt.Errorf("rev-parse --abbrev-ref: %v", err)
		}

		if buf.String() != branchName {
			cmd := exec.Command("git", "checkout", branchName)
			if err := WithDir(cmd, p).Run(); err != nil {
				return "", fmt.Errorf("git checkout %s: %v", branchName, err)
			}
		}

		cmd = exec.Command("git", "rev-parse", "HEAD")
		cmd.Dir = p
		var buf2 bytes.Buffer
		cmd.Stdout = &buf2
		if err := cmd.Run(); err != nil {
			return "", fmt.Errorf("git rev-parse Head: %v", err)
		}
		newSHA = strings.TrimSpace(buf2.String())
	}

	return newSHA, nil
}
