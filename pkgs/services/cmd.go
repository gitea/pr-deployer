package services

import (
	"os/exec"
	"path/filepath"
	"strconv"

	"gitea.com/gitea/pr-deployer/pkgs/settings"
)

func getPRDir(number int) string {
	return filepath.Join(settings.CodeCacheDir, strconv.Itoa(number))
}

func getPRGitDir(number int) string {
	return filepath.Join(getPRDir(number), "git")
}

func getPRRuntimeDir(number int) string {
	return filepath.Join(getPRDir(number), "container")
}

func WithDir(cmd *exec.Cmd, dir string) *exec.Cmd {
	cmd.Dir = dir
	return cmd
}
