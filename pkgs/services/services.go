package services

import (
	"context"
	"fmt"
	"os"

	"gitea.com/gitea/pr-deployer/pkgs/settings"
	log "github.com/sirupsen/logrus"
)

const (
	statusPending = "pending"
	statusSuccess = "success"
	statusError   = "error"
	statusFailure = "failure"
)

func UpdateAndStartPullRequest(ctx context.Context, client Client, number int, sha string) error {
	log.Trace("CheckWebhook")
	if err := client.CheckWebhook(ctx); err != nil {
		return err
	}

	prDir := getPRDir(number)
	if err := os.MkdirAll(prDir, os.ModePerm); err != nil {
		return err
	}

	// 0 download the git
	log.Trace("updateGitRepo")

	newSHA, err := updateGitRepo(number, sha)
	if err != nil {
		return err
	}

	log.Trace("UpdateCommitStatus pending", newSHA)
	// 1 send commit status
	if err := client.UpdateCommitStatus(ctx, number, newSHA, statusPending, "", ""); err != nil {
		return err
	}

	if err := updateAndStartPullRequest(ctx, client, number, newSHA); err != nil {
		log.Errorf("UpdateCommitStatus %s failure: %v", newSHA, err)
		if err := client.UpdateCommitStatus(ctx, number, newSHA, statusFailure, err.Error(), ""); err != nil {
			return err
		}
	}
	return nil
}

func updateAndStartPullRequest(ctx context.Context, client Client, number int, newSHA string) error {
	log.Trace("checkAndUpdateSubDomain")
	// 2 change domain
	if err := checkAndUpdateSubDomain(number); err != nil {
		return err
	}

	log.Trace("buildImage")
	// 3 build
	image, err := buildImage(ctx, number)
	if err != nil {
		return err
	}

	log.Trace("runImage", image)
	// 4 change reverse server
	runID, err := runImage(ctx, number, image)
	if err != nil {
		return err
	}

	log.Trace("UpdateCommitStatus success", newSHA, runID)
	// 5 send commit status
	if err := client.UpdateCommitStatus(ctx, number, newSHA, statusSuccess, "", fmt.Sprintf("https://try-pr-%d.%s", number, settings.PRParentDomain)); err != nil {
		return err
	}

	return nil
}

func StopPullRequest(number int) error {
	return nil
}
