package services

import (
	"context"
	"fmt"

	"gitea.com/gitea/pr-deployer/pkgs/settings"
	"github.com/cloudflare/cloudflare-go"
	"github.com/google/go-github/v39/github"
)

func getSubDomainName(number int) string {
	return fmt.Sprintf("try-pr-%d", number)
}

func getFullSubDomain(number int) string {
	return getSubDomainName(number) + "." + settings.PRParentDomain
}

func checkAndUpdateSubDomain(number int) error {
	api, err := cloudflare.NewWithAPIToken(settings.CloudflareToken)
	if err != nil {
		return err
	}

	zoneID, err := api.ZoneIDByName(settings.PRParentDomain)
	if err != nil {
		return fmt.Errorf("ZoneIDByName: %v", err)
	}

	var found bool
	var name = getSubDomainName(number)
	var filter = cloudflare.DNSRecord{
		Type: "A",
		Name: getFullSubDomain(number),
	}
	recs, err := api.DNSRecords(context.Background(), zoneID, filter)
	if err != nil {
		return fmt.Errorf("DNSRecords: %v", err)
	}

	for _, r := range recs {
		if filter.Name == r.Name {
			found = true
			break
		}
	}
	if found {
		// TODO: check ip address
		return nil
	}

	asciiInput := cloudflare.DNSRecord{
		Type:      "A",
		Name:      name,
		Content:   settings.DomainIP,
		Proxiable: false,
		Proxied:   github.Bool(false),
	}

	_, err = api.CreateDNSRecord(context.Background(), zoneID, asciiInput)
	if err != nil {
		return fmt.Errorf("CreateDNSRecord: %v", err)
	}
	return nil
}
