package settings

import (
	"errors"
	"fmt"
	"net/url"
	"os"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"golang.org/x/oauth2"
)

var (
	ServiceType        string
	RepoURL            string
	BaseURL            string
	RepoOwner          string
	RepoName           string
	OAuth2CallbackURL  string
	OAuth2ServerURL    string
	OAuth2ClientID     string
	OAuth2ClientSecret string
	Domain             string
	DomainIP           string
	PRParentDomain     string
	WebhookSecretKey   []byte
	CodeCacheDir       string
	CloudflareToken    string
	CloudflareEmail    string
	Proxy              struct {
		Enabled       bool
		ProxyURL      string
		ProxyURLFixed *url.URL
		ProxyHosts    []string
	}

	DBType    string
	DBConnStr string

	DockerComposeTemplateFile string
)

var (
	OAuth2Conf *oauth2.Config
)

func Init() error {
	viper.SetConfigFile("./config.yaml")
	if err := viper.ReadInConfig(); err != nil {
		return err
	}

	ServiceType = viper.GetString("ServiceType")
	if ServiceType == "" {
		return errors.New("ServiceType cannot be empty")
	}
	if !strings.EqualFold(ServiceType, "github") && !strings.EqualFold(ServiceType, "gitea") {
		return fmt.Errorf("unknow service type: %s", ServiceType)
	}

	RepoURL = viper.GetString("RepoURL")
	if RepoURL == "" {
		return errors.New("RepoURL cannot be empty")
	}

	u, err := url.Parse(RepoURL)
	if err != nil {
		return err
	}

	BaseURL = u.Scheme + "://" + u.Host
	var p = u.Path
	if strings.HasPrefix(p, "/") {
		p = p[1:]
	}
	fields := strings.Split(p, "/")
	RepoOwner = fields[0]
	RepoName = fields[1]

	OAuth2CallbackURL = viper.GetString("OAuth2.CallbackURL")
	if OAuth2CallbackURL == "" {
		return errors.New("OAuth2CallbackURL cannot be empty")
	}
	OAuth2ServerURL = BaseURL

	OAuth2ClientID = viper.GetString("OAuth2.ClientID")
	if OAuth2ClientID == "" {
		return errors.New("OAuth2ClientID cannot be empty")
	}
	OAuth2ClientSecret = viper.GetString("OAuth2.ClientSecret")
	if OAuth2ClientSecret == "" {
		return errors.New("OAuth2ClientSecret cannot be empty")
	}

	OAuth2Conf = &oauth2.Config{
		ClientID:     OAuth2ClientID,
		ClientSecret: OAuth2ClientSecret, // change this to your gitea secret id
		Endpoint: oauth2.Endpoint{
			TokenURL: fmt.Sprintf("%s/login/oauth/access_token", OAuth2ServerURL),
			AuthURL:  fmt.Sprintf("%s/login/oauth/authorize", OAuth2ServerURL),
		},
		RedirectURL: fmt.Sprintf("%s/callback", OAuth2CallbackURL),
		Scopes:      []string{"repo", "admin:repo_hook"},
	}

	Domain = viper.GetString("Domain")
	DomainIP = viper.GetString("DomainIP")
	PRParentDomain = viper.GetString("PRParentDomain")
	if PRParentDomain == "" {
		return errors.New("PRParentDomain should not be empty")
	}

	WebhookSecretKey = []byte(viper.GetString("WebhookSecretKey"))
	CodeCacheDir = viper.GetString("CodeCacheDir")
	CodeCacheDir, err = filepath.Abs(CodeCacheDir)
	if err != nil {
		return err
	}
	if err := os.MkdirAll(CodeCacheDir, os.ModePerm); err != nil {
		return err
	}

	CloudflareToken = viper.GetString("CloudflareToken")
	if CloudflareToken == "" {
		return errors.New("cloudflare token should not be empty")
	}
	CloudflareEmail = viper.GetString("CloudflareEmail")
	if CloudflareEmail == "" {
		return errors.New("cloudflare email should not be empty")
	}

	Proxy.Enabled = viper.GetBool("Proxy.Enabled")
	Proxy.ProxyURL = viper.GetString("Proxy.ProxyURL")
	if Proxy.ProxyURL != "" {
		var err error
		Proxy.ProxyURLFixed, err = url.Parse(Proxy.ProxyURL)
		if err != nil {
			return err
		}
	}
	Proxy.ProxyHosts = viper.GetStringSlice("Proxy.ProxyHosts")

	DBType = viper.GetString("DBType")
	if DBType == "" {
		DBType = "mysql"
	}
	DBConnStr = viper.GetString("DBConnStr")
	if DBConnStr == "" {
		return errors.New("DBConnStr is empty")
	}
	log.Tracef("DBType: %v DBConnStr: %v", DBType, DBConnStr)

	DockerComposeTemplateFile = viper.GetString("DockerComposeTemplateFile")
	if DockerComposeTemplateFile == "" {
		return errors.New("DockerComposeTemplateFile should not be empty")
	}

	DockerComposeTemplateFile, err = filepath.Abs(DockerComposeTemplateFile)
	if err != nil {
		return err
	}

	return nil
}
