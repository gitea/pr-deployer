package cmd

import (
	"fmt"
	"io/fs"

	"github.com/spf13/cobra"
)

var (
	// Version means the version of reverse
	Version = "0.1+dev"

	versionFlag *bool

	deployerCmd = &cobra.Command{
		Version: Version,
		Use:     "pr-deployer",
		Short:   "PR-Deployer is a tool to help deploy PR",
		Long:    `PR-Deployer is a tool to help deploy PR`,
		Run: func(cmd *cobra.Command, args []string) {
			if versionFlag != nil && *versionFlag {
				fmt.Printf("%s\n", Version)
				return
			}

			runWeb(cmd, args)
		},
	}

	publicFS   fs.FS
	templateFS fs.FS
)

func init() {
	versionFlag = deployerCmd.Flags().BoolP("version", "v", false, "version of the tool")
}

// Execute represnets execute command
func Execute(pFS, tFS fs.FS) error {
	publicFS = pFS
	templateFS = tFS
	return deployerCmd.Execute()
}
