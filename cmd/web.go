package cmd

import (
	"github.com/spf13/cobra"

	"gitea.com/gitea/pr-deployer/routers"
)

func runWeb(cmd *cobra.Command, args []string) {
	routers.Web(publicFS, templateFS)
}
