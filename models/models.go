package models

import (
	"fmt"

	"xorm.io/xorm"

	"gitea.com/gitea/pr-deployer/pkgs/settings"

	_ "github.com/go-sql-driver/mysql"
)

var engine *xorm.Engine

func Init() error {
	var err error
	engine, err = xorm.NewEngine(settings.DBType, settings.DBConnStr)
	if err != nil {
		return fmt.Errorf("NewEngine: %v", err)
	}

	return engine.Sync(new(SystemConfig))
}
