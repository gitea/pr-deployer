package models

import "time"

type SystemConfig struct {
	ID      int64
	Name    string    `xorm:"unique"`
	Value   string    `xorm:"TEXT"`
	Created time.Time `xorm:"created"`
	Updated time.Time `xorm:"updated"`
}

func GetSystemConfig(name string) (string, error) {
	var cfg SystemConfig
	has, err := engine.Where("name=?", name).Get(&cfg)
	if err != nil {
		return "", err
	}
	if !has {
		return "", nil
	}
	return cfg.Value, nil
}

func SaveSystemConfig(name, value string) error {
	var cfg SystemConfig
	has, err := engine.Where("name=?", name).Get(&cfg)
	if err != nil {
		return err
	}
	if !has {
		cfg.Name = name
		cfg.Value = value
		_, err = engine.Insert(&cfg)
		return err
	}

	_, err = engine.Where("name=?", name).Update(&SystemConfig{
		Value: value,
	})
	return err
}
